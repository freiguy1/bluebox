import os
import time
import os
import time
import ssl
import wifi
import socketpool
import microcontroller
import board
import busio
import digitalio
import adafruit_requests
import adafruit_dht
from adafruit_io.adafruit_io import IO_HTTP, AdafruitIO_RequestError


min_temp = 65
turn_on_heater = min_temp - 2
turn_off_heater = min_temp + 2
roof_state = 1 # 1: closed     2: half open     3: full open
close_roof_temp = 78
open_2_temp = 84
close_2_temp = 84
open_3_temp = 91

aio_username = os.getenv('ADAFRUIT_IO_USERNAME')
aio_key = os.getenv('ADAFRUIT_IO_KEY')

is_connected = False
io = None

dht = adafruit_dht.DHT22(board.GP15)
heater = digitalio.DigitalInOut(board.GP16)
heater.direction = digitalio.Direction.OUTPUT
roof_pin_1 = digitalio.DigitalInOut(board.GP17)
roof_pin_1.direction = digitalio.Direction.OUTPUT
roof_pin_2 = digitalio.DigitalInOut(board.GP18)
roof_pin_2.direction = digitalio.Direction.OUTPUT

def wifi_io_connect():
    global io
    global is_connected
    try:
        wifi.radio.connect(os.getenv('WIFI_SSID'), os.getenv('WIFI_PASSWORD'))

        pool = socketpool.SocketPool(wifi.radio)
        requests = adafruit_requests.Session(pool, ssl.create_default_context())
        # Initialize an Adafruit IO HTTP API object
        io = IO_HTTP(aio_username, aio_key, requests)
        is_connected = True
        print("connected to io")
    except:
        is_connected = False
        print("Unable to connect")
        
def close_roof():
    global roof_state
    global roof_pin_1
    global roof_pin_2
    roof_pin_1.value = True
    roof_pin_2.value = False
    roof_state = 1

def open_roof_2():
    global roof_state
    global roof_pin_1
    global roof_pin_2
    roof_pin_1.value = False
    roof_pin_2.value = True
    time.sleep(3) # 3 seconds to open appropriate amount
    roof_pin_1.value = False
    roof_pin_2.value = False
    roof_state = 2

def close_roof_2():
    global roof_state
    global roof_pin_1
    global roof_pin_2
    roof_pin_1.value = True
    roof_pin_2.value = False
    time.sleep(4) # 4 seconds to close appropriate amount
    roof_pin_1.value = False
    roof_pin_2.value = False
    roof_state = 2

def open_roof_3():
    global roof_state
    global roof_pin_1
    global roof_pin_2
    roof_pin_1.value = False
    roof_pin_2.value = True
    time.sleep(4) # 4 seconds to open appropriate amount
    roof_pin_1.value = False
    roof_pin_2.value = False
    roof_state = 3

####################################
#           MAIN STUFF
####################################

close_roof()
wifi_io_connect()


while True:
    # read sensor
    temp = dht.temperature
    tempF = (temp * 9 / 5) + 32
    
    #  print sensor data to the REPL
    print("\nTemperature: %0.1f F" % tempF)
    
    # manipulate heater
    if tempF < turn_on_heater:
        heater.value = True
    elif tempF > turn_off_heater:
        heater.value = False
        
    # manipulate roof
    if tempF <= close_roof_temp and roof_state > 1:
        close_roof()
    elif tempF >= open_2_temp and roof_state < 2:
        open_roof_2()
    elif tempF <= close_2_temp and roof_state > 2:
        close_roof_2()
    elif tempF >= open_3_temp and roof_state < 3:
        open_roof_3()

    # send data to feed
    if is_connected:
        try:
            io.send_data("temperature", tempF)
            
        except:
            print("exception occured, but ignoring")
    else:
        wifi_io_connect()


    #  delay
    time.sleep(60)



